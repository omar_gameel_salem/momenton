package org.omarsalem.momenton.services;

import org.omarsalem.momenton.dal.EmployeeRepository;
import org.omarsalem.momenton.models.Employee;
import org.omarsalem.momenton.viewmodels.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Node<Employee> getEmployeesAsTree() {
        final Map<Integer, Employee> employees = employeeRepository
                .getAll()
                .stream()
                .collect(Collectors.toMap(e -> e.getId(), e -> e));
        Map<Integer, List<Employee>> matrix = new HashMap<>();
        Integer ceoId = null;

        for (Map.Entry<Integer, Employee> e : employees.entrySet()) {
            final Employee employee = e.getValue();
            if (employee.getManagerId() == null) {
                if (ceoId != null) {
                    throw new IllegalStateException("More than 1 CEO found");
                }
                ceoId = employee.getId();
                continue;
            }

            if (!matrix.containsKey(employee.getManagerId())) {
                matrix.put(employee.getManagerId(), new ArrayList<>());
            }

            final List<Employee> subordinates = matrix.get(employee.getManagerId());
            subordinates.add(employee);
            matrix.put(employee.getManagerId(), subordinates);
        }

        if (ceoId == null) {
            throw new IllegalStateException("No CEO found");
        }

        final Employee ceo = employees.get(ceoId);

        final Node<Employee> root = new Node<>(ceo);
        createTree(root, matrix, 0);

        return root;
    }

    private void createTree(Node<Employee> root, Map<Integer, List<Employee>> employees, int level) {
        root.setLevel(level);
        final List<Employee> subordinates = employees
                .get(root.getData().getId());
        if (subordinates == null) {
            return;
        }
        root.setChildren(subordinates
                .stream()
                .map(employee -> new Node<>(employee))
                .collect(Collectors.toList()));

        for (Node<Employee> child : root.getChildren()) {
            createTree(child, employees, level + 1);
        }
    }
}
