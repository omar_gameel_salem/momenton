package org.omarsalem.momenton.services;

import org.omarsalem.momenton.models.Employee;
import org.omarsalem.momenton.viewmodels.Node;

public interface EmployeeService {
    Node<Employee> getEmployeesAsTree();
}
