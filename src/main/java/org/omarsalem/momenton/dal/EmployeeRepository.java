package org.omarsalem.momenton.dal;

import org.omarsalem.momenton.models.Employee;

import java.util.ArrayList;

public interface EmployeeRepository {
    ArrayList<Employee> getAll();
}
