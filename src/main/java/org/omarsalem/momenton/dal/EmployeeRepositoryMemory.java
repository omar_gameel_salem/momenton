package org.omarsalem.momenton.dal;

import org.omarsalem.momenton.models.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class EmployeeRepositoryMemory implements EmployeeRepository {
    private final ArrayList<Employee> employees;

    public EmployeeRepositoryMemory() {
        employees = new ArrayList<>();
        employees.add(new Employee("Alan", 100, 150));
        employees.add(new Employee("Martin", 220, 100));
        employees.add(new Employee("Jamie", 150));
        employees.add(new Employee("Alex", 275, 100));
        employees.add(new Employee("Steve", 400, 150));
        employees.add(new Employee("David", 190, 400));
    }

    @Override
    public ArrayList<Employee> getAll() {
        return employees;
    }
}
