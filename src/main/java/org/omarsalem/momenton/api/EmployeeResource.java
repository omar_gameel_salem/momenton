package org.omarsalem.momenton.api;

import io.swagger.annotations.Api;
import org.omarsalem.momenton.models.Employee;
import org.omarsalem.momenton.services.EmployeeService;
import org.omarsalem.momenton.viewmodels.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "employees")
@RequestMapping("/employees")
public class EmployeeResource {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeResource(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @RequestMapping(value = "/visualize", method = RequestMethod.GET)
    @ResponseBody
    public String visualize() {
        final Node<Employee> root = employeeService.getEmployeesAsTree();
        return root.print();
    }
}
