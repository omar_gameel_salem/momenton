package org.omarsalem.momenton.viewmodels;

import java.util.ArrayList;
import java.util.List;

public class Node<T> {
    private T data;
    private List<Node<T>> children;
    private int level;

    public Node(T data) {
        this(data, new ArrayList<>(), 0);
    }

    public Node(T data, int level) {
        this(data, new ArrayList<>(), level);
    }

    public Node(T data, List<Node<T>> children) {
        this(data, children, 0);
    }

    public Node(T data, List<Node<T>> children, int level) {
        this.data = data;
        this.children = children;
        this.level = level;
    }

    public List<Node<T>> getChildren() {
        return children;
    }

    public void setChildren(List<Node<T>> children) {
        this.children = children;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node<?> node = (Node<?>) o;

        return data.equals(node.data);

    }

    @Override
    public int hashCode() {
        return data.hashCode();
    }

    public String print() {
        StringBuilder sb = new StringBuilder();
        printRecursive(sb, this);
        return sb.toString();
    }

    private void printRecursive(StringBuilder sb, Node<T> root) {
        for (int i = 0; i < root.level; i++) {
            sb.append("\t");
        }
        sb.append(root.getData().toString());
        sb.append("\n");
        for (Node<T> child : root.getChildren()) {
            printRecursive(sb, child);
        }
    }
}
