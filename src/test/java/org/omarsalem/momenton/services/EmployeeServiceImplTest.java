package org.omarsalem.momenton.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.omarsalem.momenton.dal.EmployeeRepository;
import org.omarsalem.momenton.dal.EmployeeRepositoryMemory;
import org.omarsalem.momenton.models.Employee;
import org.omarsalem.momenton.services.EmployeeService;
import org.omarsalem.momenton.viewmodels.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceImplTest {

    private final static Comparator<Node<Employee>> COMP = (Node<Employee> o1, Node<Employee> o2) -> (o1.getData().compareTo(o2.getData()));


    @MockBean
    private EmployeeRepository employeeRepositoryMock;

    @Autowired
    private EmployeeService target;

    @Test
    public void getEmployeesAsTree_validData_treeCreated() {
        //Arrange
        Employee alan = new Employee("Alan", 100, 150);
        Employee martin = new Employee("Martin", 220, 100);
        Employee jamie = new Employee("Jamie", 150);
        Employee alex = new Employee("Alex", 275, 100);
        Employee steve = new Employee("Steve", 400, 150);
        Employee david = new Employee("David", 190, 400);

        Node<Employee> alanNode = new Node<>(alan, asList(new Node<>(martin), new Node<>(alex)));
        Node<Employee> steveNode = new Node<>(steve, asList(new Node<>(david)));
        Node<Employee> root = new Node<>(jamie, asList(steveNode, alanNode));

        when(employeeRepositoryMock.getAll()).thenReturn(new EmployeeRepositoryMemory().getAll());

        //Act
        final Node<Employee> actual = target.getEmployeesAsTree();

        //Assert
        final boolean treesAreEqual = checkTreesAreEqual(actual, root);
        assertTrue(treesAreEqual);
    }

    @Test(expected = IllegalStateException.class)
    public void getEmployeesAsTree_emptyList_exceptionThrown() {
        //Arrange
        when(employeeRepositoryMock.getAll()).thenReturn(new ArrayList<>());

        //Act
        target.getEmployeesAsTree();
    }

    @Test(expected = IllegalStateException.class)
    public void getEmployeesAsTree_moreThanOneCEO_exceptionThrown() {
        //Arrange
        final List<Employee> ceos = Arrays.asList(new Employee("ceo1", 1), new Employee("ceo2", 2));
        when(employeeRepositoryMock.getAll()).thenReturn(new ArrayList<>(ceos));

        //Act
        target.getEmployeesAsTree();
    }

    @Test(expected = IllegalStateException.class)
    public void getEmployeesAsTree_noCEOPresent_exceptionThrown() {
        //Arrange
        final List<Employee> employees = Arrays.asList(new Employee("emp1", 1, 10), new Employee("emp2", 2, 10));
        when(employeeRepositoryMock.getAll()).thenReturn(new ArrayList<>(employees));

        //Act
        target.getEmployeesAsTree();
    }

    private boolean checkTreesAreEqual(Node<Employee> first, Node<Employee> second) {
        if (first == null && second == null) {
            return true;
        }
        if (first == null || second == null) {
            return false;
        }
        if (!first.equals(second)) {
            return false;
        }

        if (first.getChildren().size() != second.getChildren().size()) {
            return false;
        }

        first.getChildren().sort(COMP);
        second.getChildren().sort(COMP);

        for (int i = 0; i < first.getChildren().size(); i++) {

            final Node<Employee> firstNode = first.getChildren().get(i);
            final Node<Employee> secondNode = second.getChildren().get(i);

            if (!firstNode.getData().equals(secondNode.getData())) {
                return false;
            }

            if (!checkTreesAreEqual(firstNode, secondNode)) {
                return false;
            }
        }
        return true;

    }

}
