package org.omarsalem.momenton.viewmodels;

import org.junit.Assert;
import org.junit.Test;
import org.omarsalem.momenton.models.Employee;

import static java.util.Arrays.asList;


public class NodeTest {

    @Test
    public void print() throws Exception {
        //Arrange
        Employee alan = new Employee("Alan", 100, 150);
        Employee martin = new Employee("Martin", 220, 100);
        Employee jamie = new Employee("Jamie", 150);
        Employee alex = new Employee("Alex", 275, 100);
        Employee steve = new Employee("Steve", 400, 150);
        Employee david = new Employee("David", 190, 400);

        Node<Employee> alanNode = new Node<>(alan, asList(new Node<>(martin, 2), new Node<>(alex, 2)), 1);
        Node<Employee> steveNode = new Node<>(steve, asList(new Node<>(david, 2)), 1);
        Node<Employee> root = new Node<>(jamie, asList(steveNode, alanNode), 0);

        String expected = "Jamie\n" +
                "\tSteve\n" +
                "\t\tDavid\n" +
                "\tAlan\n" +
                "\t\tMartin\n" +
                "\t\tAlex\n";

        //Act
        final String actual = root.print();

        //Assert
        Assert.assertEquals(expected, actual);
    }
}