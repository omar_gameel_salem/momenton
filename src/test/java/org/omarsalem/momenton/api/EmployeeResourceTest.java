package org.omarsalem.momenton.api;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omarsalem.momenton.models.Employee;
import org.omarsalem.momenton.services.EmployeeService;
import org.omarsalem.momenton.viewmodels.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeResourceTest {

    @MockBean
    private EmployeeService employeeService;

    @Autowired
    WebApplicationContext wac;

    private MockMvc api;

    @Before
    public void setup() {
        this.api = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void visualize() throws Exception {
        //Arrange
        final Node<Employee> root = (Node<Employee>) mock(Node.class);
        final String expected = "print";
        when(root.print()).thenReturn(expected);
        when(employeeService.getEmployeesAsTree()).thenReturn(root);

        //Act
        final String actual = api.perform(
                MockMvcRequestBuilders
                        .get("/employees/visualize"))
                .andReturn()
                .getResponse()
                .getContentAsString();

        //Assert
        Assert.assertEquals(expected, actual);
    }
}